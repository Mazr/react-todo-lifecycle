import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      buttonState:"Show"
    };
    this.changeState = this.changeState.bind(this);
    this.getToList = this.getToList.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);
  }

  render() {
    return(
      <div className='App'>
        <button className={"blue-button"} onClick={this.changeState}>
          {this.state.buttonState}
        </button>
        <button className={"blue-button"} onClick={this.handleRefresh}>
          Refresh
        </button>
        {this.getToList()}
      </div>)
  }

  changeState(){
    this.setState({buttonState: ((this.state.buttonState === "Show") ? "Hide" : "Show")})
  }
  getToList() {
    if (this.state.buttonState === "Hide"){
      return <TodoList/>;
    }
  }
  handleRefresh() {
    location.reload();
  }

};

export default App;