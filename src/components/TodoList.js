import React, {Component} from 'react';
import './todolist.less';

class TodoList extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      index: 1,
      itemList: []
    }

    console.log("constructor()");
    this.addItem = this.addItem.bind(this);
  }

  render() {
    console.log("render()");
    return (
      <div>
        <button className={"grey-button"} onClick={this.addItem}>Add</button>
        <div>
          {this.state.itemList}
        </div>
      </div>
    );
  }

  addItem() {
    this.state.itemList.push(<div className={"item"}> {"List Item " + this.state.index}</div>);
    this.setState({index: this.state.index + 1});
  }
  componentDidMount() {
    console.log("componentDidMount()");
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("componentDidUpdate()");
  }
  componentWillUnmount() {
    console.log("componentWillUnmount()");
  }

}

export default TodoList;